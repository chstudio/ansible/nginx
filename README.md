Ansible Role to manage Nginx installation
-----------------------------------------

This simple role allow you to install nginx on a specific machine.

## Example

```ansible-playbook
- name: Install Nginx
  include_role:
    name: nginx
```
